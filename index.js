document.getElementById("btn-1").addEventListener('click', () => {
	alert("Add More!");
});

let paragraph = document.getElementById("paragraph-1");
let paragraph2 = document.getElementById("paragraph-2");

document.getElementById("btn-2").addEventListener('click', 
	() => { 
		paragraph.innerHTML = "I can even do this!";
});

document.getElementById("btn-3").addEventListener('click', 
	()=>{
	paragraph2.innerHTML = "Or this!";
	paragraph2.style.color = "purple";
	paragraph2.style.fontSize = "50px";
});

/*Lesson Proper
	Topics
		- Introduction
		- Writing Comments
		- Syntax and Statements
		- Storing Values through varaibles
		- Peeking at Variable's value through console
		- Data types
		- Functions

Javascript
	- is a scripting language the enables you to make interactive web pages
	- it is one core technologies of the World Wide Web
	- was originally intended for web browsers. However, they are now also 
	integrated in web servers through the use of Node.js.

Uses of Javascript
	- Web app development
	- Browser-based game development
	- Art Creation
	- Mobile applications
*/

// Writing Comments in JavaScript:
// There are two ways of writing comments in JS:
		// - single line comments - ctrl + /
			// sample comments - can only make comments in single line
		/*
			multi-line comments:
				ctrl + shift + /
			comments in JS, much like CSS and HTML, is not read by the browser. So,
			these comments are often used to add notes and to add markers to your
			code.
		*/
console.log("Hello, my name is Thonie. I purple you.");
/*
	JavaScript
		we can see or log message in our console.

		- Consoles are part of our browser which will allow us to see/log messsages,
		data or information from our programming language.

		- for most browsers, console can be accessed through its developer tools in
		the console tab/

		In fact, consoles in browser allow us to add some JavaScript expression.

	Statements
		- Statements are instructionm expressions we add to our programming language
		which will then be communicated to our computers
		- Statement in JavaScript commonly ends in semi-colon( ;). However, JavaScript
		has an implemented way of automatically adding semicolons at the end of our
		statements. Which, therefore, mean that, unlike other languages, JS does NOT
		require semicolons.
		- Semicolons in js are mostly used to mark the end of the statement

	Syntax
		- Syntax in programming, is a set of rules that describes how statements 
		are properly made/constructed

		- lines/blocks of code must follow a certain rules for it to work. Because
		remember, you are not merely communicating with another human, in fact you
		are communicating with a computer.
*/

/*Variables*/
/*
	In HTML, elements are containers of other element and text.
	In JavaScript, variables are containers of data. A given name is used to describe
	that piece of data.

	Variables also allows us to use or refer to data multiple times
*/

//num is our variable
//10 being the value/data


let num = 10;
console.log(6);
console.log(num);

let name = "Jin";
console.log("V");
console.log(name);

/* Ceating variables 

	To create a variable, there are two steps to be done:
	-Declaration which actually allows to create the varaibale 
	-Initialization which allows to add an initial value to a variable

	Variables in JS are decleared with the use of let or const keyword.

*/

let myVariable;

/*We can create variables without an initial value. However, when logged into the 
console, the variable will return a value of undefined. 

Undefined is a dataype that indicates that variable does
exist, however there was no initial value.*/

console.log(myVariable);

myVariable = "New Initilized Value";
console.log(myVariable);


/*
myVariable2 = "Initial Value"
let myVariable2;
console.log (myVariable2);

Note: You cannot and should not access a variable before it's been 
created/declared

you cannot use or refer to a variable that has not been declared or crated.


Undefined vs Not defined

	Undefined
		 means that a variable has been declared but there is no
		initial value.
			-undefined is a data type.

	Not defined 
		means that the variable you are trying to refer or access does
		not exist

			- not defined is an error.

	Note:
		Some errors in JS, will stop the program from
		further executing.

*/


let myVariable3 = "Another sample";
console.log(myVariable3);

/* Variables must be declared first before they are used, referred 
or accessed.

Using, referrring to or accessing a variable before it's been declared 
results an error. */


/*
Let vs Const

Let 
	we can create vaariable that can be declared, initiliazed and 
	re-assigned. In fact, we can declare let variables and 
	initialized after.

*/

let bestFinalFantasy;
bestFinalFantasy = "Final Fantasy 6";
console.log(bestFinalFantasy);

/*re-assigning let variables */

bestFinalFantasy = "Final Fantasy 7";
console.log(bestFinalFantasy);

/* The value changed. Meaning we can reassign values to let variables*/

const pi = 3.1416;

console.log(pi);

/*
const variables 
	are variables with constant data. Therefore we should not redeclare.
	Additionally, you cannot reassign another value to a cons variable.

const mvp = "Michael Jordan";
console.log(mvp);

mvp = "Lebron James";
console.log(mvp);
*/

/* Guides on Variable Names

1. When naming variables, it is important to create variables
	that are descriptive and indicative of data it contains.

2. When naming variables, it is better to start with a lowercase
	letter. We usually avoid creating variable names that starts with
	capital letters, because there are several keywords in JS that starts
	in capital letters.

3. Do not add to your variable names. Use camelCase for multiple
	words, or underscore.

*/

let num_sum = 5000;
let numSum = 3000;

console.log(num_sum);
console.log(numSum);


let breand = "Toyota", model = "Vios", type = "Sedan";
console.log(breand);
console.log(model);
console.log(type);

console.log(breand,model,type);

/*
Data types 
	In most programming Languages, data is differntiated by
	their types. For most programming languages, you have to 
	declare not the variable name but also the type of data
	you are saving into a variable. However, JS does not require this.

	To create data with particular data types, some data types rquire 
	adding with literal.

		string literals = ' ', "" and most recently: ``(
		template litearals)

		object literals = {}
		array literals = []

*/


/*
Strings
	Are a series of alphanumeric characters that cerate a word,
	a phrase, a name or anything related to creating text.

		String literals such as ''(single quote) or ""(double quotes)
		are used to write or create strings.



*/



let first_name = "John Dave";
let last_name = "Umban";

console.log(first_name, last_name);

/*
Concatenation is a process/operation wherein we
combine two or more strings as one. Using (+) sign.

	in JS, spaces are also counted as characters

	console.log(first_name +" "+last_name);

*/

let word = "I";
let word1 ="am";
let word2 = ".";
let word3 = "a"
let word4 = "student";
let word5 = "De La Salle"
let word6 = " University";
let word7 = "Dasmarinas"
let word8 = "of";

let space = " ";

let sentence = word + space + word1 + space + first_name + space +
last_name + word2 + space + word + 
space + word1 + space + word3 + space + word4 + space+ word8 +space +  word5 + space + word6 + space +  word7;

console.log(sentence);	

sentence = `${first_name} ${word} ${word1} ${word3} ${word4} 
${word5} ${word6} ${word7} ${word4}`;

console.log(sentence);


Kusogaki
Gabriel Naty
/* Number (Data type)
	Integers (whole numbers) and floats (decimals). These are our number data
	which can be used for mathematical operations
*/

	let numString1 = "5";
	let numString2 = "6";
	let num1 = 5;
	let num2 = 6;
	console.log(numString1 + numString2);//56 strings were concatenated
	console.log(num1 + num2);/*11 - both argument in the operation are number 
	types*/
	let num3 = 5.5;
	let num4 = .5;
	console.log(num1 + num3);
	console.log(num3 + num4);

	/* When the + or addition operator is used on numbers, it will do the
	proper mathematical operation. However, when used on strings, it will
	concatenate. */

	console.log(numString1 + num1);//55 - resulted in concatenation
	console.log(num3 + numString2);
	/*Forced coercion - when one data's type is forced to change to complete
	an operation*/
	// string + num = concatenation

	/* parseInt() - this can change the type of numeric string to a proper 
	number*/
	console.log(parseInt(numString1) + num1);/*10 - numString1 was parsed 
	into a proper number*/